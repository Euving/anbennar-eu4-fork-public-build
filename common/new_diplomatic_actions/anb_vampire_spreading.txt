vampire_spread_action = {
	category = covert
	
	is_visible = {
		ai = no
		has_estate = estate_vampires
		FROM = {
			capital_scope = {
				OR = {
					continent = europe
					range = ROOT
				}
			}
			NOT = {
				has_country_flag = has_vampires
				has_country_flag = no_free_estate_slots
			}
		}
    }
	
	is_allowed = {
		ROOT = {
			reverse_has_opinion = {
				who = FROM
				value = 50
			}
			has_spy_network_in = {
				who = FROM
				value = 50
			}
			estate_territory = {
    			estate = estate_vampires
    			territory = 2
			}
		}
		ROOT = {
			has_estate = estate_vampires
			NOT = {has_estate_privilege = estate_vampires_organization_vampiric_emigres}
			has_estate_privilege = estate_vampires_law_state_collusion_masquerade
			estate_influence = {
    			estate = estate_vampires
    			influence = 60
			}
		}
		FROM = {
			capital_scope = {
				OR = {
					continent = europe
					range = ROOT
				}
			}
			NOT = {
				has_country_flag = has_vampires
				has_country_flag = no_free_estate_slots
			}
		}
	}

    on_accept = {
		#emigres arival event
		FROM = {
			country_event = { id = vampires_estate_events.11 days = 3 }
		}
		ROOT = {
			change_estate_land_share = {
				estate = estate_vampires
				share = -1
			}
			add_spy_network_in = {
   				who = FROM
    			value = -50
			}
		}
	}
	ai_will_do = {
		always = no
	}
}
