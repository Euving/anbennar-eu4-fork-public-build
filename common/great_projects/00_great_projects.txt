marrhold_tunnel = {
	#ambient_object = panama_canal
	province = 6502
	is_canal = yes
	time = 180
	modifier = {
		province_trade_power_value = 35 #Doesnt do shit
	}
}

city_canal = {
    #ambient_object = suez_canal
    province = 5304
    is_canal = yes
    time = 120
    
    # Applied to the province defined above on project completion
    modifier = {
        province_trade_power_value = 20
    }
}

duty = {
    ambient_object = duty
    province = 5253
    is_canal = no
    time = 120
    
    # Applied to the province defined above on project completion
    modifier = {
		global_unrest = -1
    }
}

redrushes_climb = {
	#ambient_object = panama_canal
	province = 1808
	is_canal = yes
	time = 50
	modifier = {
	}
}

spoorland_lift = {
	#ambient_object = panama_canal
	province = 1797
	is_canal = yes
	time = 50
	modifier = {
	}
}

walkway_of_thorns = {
	#ambient_object = panama_canal
	province = 6510
	is_canal = yes
	time = 50
	modifier = {
	}
}

arca_noruin = {
	#ambient_object = panama_canal
	province = 1783
	is_canal = yes
	time = 50
	modifier = {
	}
}

arca_venaan = {
	#ambient_object = panama_canal
	province = 1801
	is_canal = yes
	time = 50
	modifier = {
	}
}

arbeloch_ascensor = {
	#ambient_object = panama_canal
	province = 6502
	is_canal = yes
	time = 50
	modifier = {
	}
}